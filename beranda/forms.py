from django import forms

class statusBar (forms.Form):
	status = forms.CharField(label='Ceritakan harimu!')

class subscribe(forms.Form):
	subs_name = {'type': 'text', 'class': 'form-control', 'placeholder':'Enter Your Fullname',}
	name = forms.CharField(max_length = 100, error_messages={"required": "Please enter your name"}, widget = forms.TextInput(attrs=subs_name))
	subs_email = {'type': 'text', 'class': 'form-control', 'placeholder':'example@email.com',}
	email = forms.EmailField(error_messages={"required": "Please enter a valid email"}, max_length= 100, widget=forms.TextInput(attrs=subs_email))
	subs_pass = {'type': 'password', 'class': 'form-control', 'placeholder':'Enter Your Password',}
	password = forms.CharField(max_length = 20, min_length= 8, widget = forms.PasswordInput(attrs=subs_pass), error_messages={"min_length": "password must be longer than 7 characters"})
	