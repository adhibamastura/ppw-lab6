from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from .forms import statusBar
from .models import statusBar2
from .forms import subscribe
from .models import subscriber  
from django.http import HttpResponseRedirect, JsonResponse
from django.contrib.auth import authenticate, login, logout
from django.views.decorators.csrf import csrf_exempt
import urllib.request, json 
import requests

# Create your views here.
def updateStatus(request):
    response = {'form': statusBar, 'model': statusBar2.objects.all()}
    return render(request, 'updateStatus.html', response)

def newStatus(request):
    response = {}
    if request.method == 'POST':
        form = statusBar(request.POST)
        obj = statusBar2()
        obj.status = request.POST['status']
        obj.save()
        return HttpResponseRedirect(reverse('updateStatus'))
    else:
        form = statusBar()
        return HttpResponseRedirect(reverse('updateStatus'))

def about(request):
    response = {}
    return render(request, 'about.html', response)

def books(request):
    response = {}
    return render(request, 'books.html', response)

def login(request):
    response = {}
    request.session['counter'] = 0
    return render(request, 'login.html', response)

def logoutPage(request):
    request.session.flush()
    logout(request)
    return HttpResponseRedirect('/books')

def plus(request):
    print(dict(request.session))
    request.session['counter'] = request.session['counter'] + 1
    return HttpResponse(request.session['counter'], content_type = "application/json")

def minus(request):
    request.session['counter'] = request.session['counter'] - 1
    return HttpResponse(request.session['counter'], content_type = "application/json")

def books_detail(request):
  url = "https://www.googleapis.com/books/v1/volumes?q=quilting"
  details = requests.get(url).json()
  return JsonResponse(details)

def home(request):
    response = {}
    response["forms"] = subscribe()
    return render(request, 'home.html', response)

@csrf_exempt
def validate(request):
    email = request.POST.get("email")
    exist = subscriber.objects.filter(email=email)
    if exist:
        data = {
            'not_valid': True
        }
    else:
        data = {
            'not_valid': False
        }
    return JsonResponse(data)

def success(request):
    submited_form = subscribe(request.POST or None)
    if (submited_form.is_valid()):
        new_data = submited_form.cleaned_data
        new_subs = subscriber(name=new_data['name'], password=new_data['password'], email=new_data['email'])
        new_subs.save()
        data = {'name': new_data['name'], 'password': new_data['password'], 'email': new_data['email']}
    return JsonResponse(data)

# def listSubscribers(request):
#     subs = subscriber.objects.all()
#     return JsonResponse({'subs': list(subs)})
