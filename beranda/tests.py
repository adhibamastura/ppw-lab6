from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import updateStatus
from .views import newStatus
from .forms import subscribe
from .models import subscriber
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import selenium.webdriver.chrome.service as service
import time


# Create your tests here.

class Story6UnitTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_using_home_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response,'home.html')

    def test_using_status_template(self):
        response = Client().get('/status')
        self.assertTemplateUsed(response,'updateStatus.html')

    def test_status_template(self):
        response = Client().get('/status')
        html_response = response.content.decode('utf8')
        self.assertIn('Ceritakan harimu!', html_response)

    def test_profile_url_exist(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code, 200)

    def test_my_name_exist(self):
        response = Client().get('/about/')
        html_response = response.content.decode('utf8')
        self.assertIn('Adhiba Mastura', html_response)

    def test_my_college_exist(self):
        response = Client().get('/about/')
        html_response = response.content.decode('utf8')
        self.assertIn('University of Indonesia', html_response)

    def test_add_url_exist(self):
        response = Client().get('/add')
        self.assertEqual(response.status_code,302)

    def test_using_update_status_template(self):
        response = Client().get('/books')
        self.assertTemplateUsed(response,'books.html')

    def test_title_exist(self):
        response = Client().get('/books')
        html_response = response.content.decode('utf8')
        self.assertIn('Books of The Month', html_response)

    def test_login_exist(self):
        response = Client().get('/books')
        login = response.content.decode('utf8')
        self.assertIn('Log in', login)
        

class Story6FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25) 
        super(Story6FunctionalTest,self).setUp()

    def test_check_input(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/status')
        title = selenium.find_element_by_name('status')
        submit = selenium.find_element_by_id('submit')
        title.send_keys('Coba-coba')
        submit.send_keys(Keys.RETURN)

    def test_position_title(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/status')
        title = selenium.find_element_by_id('apa-kabar')
        title_loc = title.location
        self.assertEqual(title_loc, {'x': 48, 'y': 104})

    def test_size_inputButton(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/status')
        inputButton = selenium.find_element_by_id('submit')
        inputButton_size = inputButton.size
        self.assertEqual(inputButton_size, {'height': 40, 'width': 81})

    # def test_check_input(self):
    #     selenium = self.selenium
    #     selenium.get('http://127.0.0.1:8000')
    #     count_before = subscriber.objects.count()
    #     form_data = {"full_name": "full name",
    #                  "email" : "email@email.com",
    #                 "password" : "password",
    #                 }
    #     self.assertEqual(subscriber.objects.count(), count_before + 1)

    def test_tag_h1(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        titleh1_align = selenium.find_element_by_tag_name('h1').value_of_css_property('text-align')
        titleh1_fontSize = selenium.find_element_by_tag_name('h1').value_of_css_property('font-size')
        self.assertEqual(titleh1_align, '-webkit-center')
        self.assertEqual(titleh1_fontSize, '80px')

    def test_navbar_color(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        navbar_color = selenium.find_element_by_id('my-navbar').value_of_css_property('background-color')
        self.assertEqual(navbar_color, 'rgba(255, 255, 255, 1)')

    def test_check_favorite(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/books')
        title = selenium.find_element_by_tag_name('h1').value_of_css_property('text-align')
        self.assertEqual(title, 'left')

    def tearDown(self):
        self.selenium.quit()
        super(Story6FunctionalTest, self).tearDown()

