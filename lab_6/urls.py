"""lab_6 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.urls import re_path
from django.conf.urls import include
from beranda.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home, name="home"),
    re_path(r'^status$', updateStatus, name='updateStatus'),
    re_path(r'^add$', newStatus, name='newStatus'),
    re_path(r'^about/$', about, name='about'),
    path('books-detail/', books_detail, name='books_detail'),
    re_path(r'^books$', books, name='books'),
    path('validate/', validate, name="validate"),
    path('success/', success, name="success"),
    path('logout/', logoutPage, name='logout'),
    path('plus', plus, name='plus'),
    path('minus', minus, name='minus'),
    path('auth/', include('social_django.urls', namespace='social')),
    # path('listSubscribers/', listSubscribers, name="listSubscribers"),
]
