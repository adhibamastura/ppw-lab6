$(document).ready(function() {
	$.ajax({
		url: "books-detail",
		datatype: 'json',
		success: function(data){
			var result ='<tr>';
			// console.log(data['items'].length)
			for(var i = 0; i < data.items.length; i++) {
				result += "<td scope='row' class='align-middle text-center'>" + (i+1) + "</td>" +
				"<td class='align-middle'>" + data.items[i].volumeInfo.title +"</td>" +
				"<td class='align-middle'> <p> <strong> Author : </strong>" + data.items[i].volumeInfo.authors + "</p>" +
				"<p> <strong> Publisher : </strong>" + data.items[i].volumeInfo.publisher + "</p>" +
				"<p> <strong> Published Date : </strong>" + data.items[i].volumeInfo.publishedDate + "</p>" + "</td>" +
				"<td><img class='img-fluid' style='width:22vh' src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>" +
				"<td class='align-middle' style='text-align:center'>" + "<img id='bintang" + i + "' onclick='favorite(this.id)' width='28px' src='https://image.flaticon.com/icons/png/512/660/660463.png'>" + "</td></tr>";
			}
			$('tbody').append(result);
		}
	})
});

var counter = 0;
function favorite(clicked_id){
	var button = document.getElementById(clicked_id);
	if(button.classList.contains("checked")){
		button.classList.remove("checked");
		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/png/512/660/660463.png';
		$.ajax({
			url: "minus",
			dataType: 'json',
			success: function(result){
				var counter = JSON.parse(result);
				console.log(result)
				document.getElementById("counter").innerHTML = counter;
				$('#counter').html(counter);
			}
		});

	}
	else{
		button.classList.add('checked');
		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/png/512/148/148839.png';
		$.ajax({
			url: "plus",
			dataType: 'json',
			success: function(result){
				var counter = JSON.parse(result);
				console.log(result)
				document.getElementById("counter").innerHTML = counter;
				$('#counter').html(counter);
			}
		});

	}
}
