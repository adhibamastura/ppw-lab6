$(document).ready(function() {
    var name = "";
    var password = "";
    var emailIsValid = false;
    $("#id_name").change(function() {
        name = $(this).val();
    });
    $("#id_email").change(function() {
        var email = $(this).val();
        $.ajax({
            url: "/validate/",
            data: {
                'email': email,
            },
            method: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data['not_valid']);
                if(data['not_valid'] == true) {
                    alert("This email already exist, please use a new one.");
                } else {
                    emailIsValid = true;
                }
                
            }
        });
    });
    $("#id_password").keyup(function() {
        password = $(this).val();
        if(name.length > 0 && password.length > 7 && emailIsValid) {
            $("#button-button-3").prop("disabled", false);
        }
    })

    $("#button-button-3").click(function() {
        event.preventDefault();
        $.ajax({
            headers: {"X-CSRFToken": $("input[name=csrfmiddlewaretoken]").val()},
            url:"/success/",
            data: $("form").serialize(),
            method: 'POST',
            dataType: 'json',
            success: function(data){
                alert("Hi! Thanks for subscribing. :)");
                document.getElementById('id_name').value = '';
                document.getElementById('id_email').value = '';
                document.getElementById('id_password').value = '';
                $("#button-button-3").prop("disabled", true);  
            }
        })
    });
    $.ajax({
        url: "listSubscribers",
        datatype: 'json',
        success: function(data){
            var result ='<tr>';
            for(var i = 0; i < data.subscriber.length; i++) {
                result += "<td scope='row' class='align-middle text-center'>" + (i+1) + "</td>" +
                "<td class='align-middle'>" + data.subscriber[i].name +"</td>" +
                "<td class='align-middle'> <p> <strong> Author : </strong>" + data.subscriber[i].email + "</p></td></tr>"
            }
            $('tbody').append(result);
        }
    })
});
